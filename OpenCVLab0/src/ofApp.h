#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void DrawCircle(cv::Mat& blackFont, float radius, float x, float y, float thick, cv::Scalar circleColor);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		cv::Mat getCvMatFromOfImage(ofImage& img);
		ofImage getOfImageFromCvImage(cv::Mat& mat);

		ofImage refImage;
		ofImage img;


};
