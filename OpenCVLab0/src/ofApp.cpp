#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	refImage.loadImage("lenna.png");

	cv::Mat cvImage = getCvMatFromOfImage(refImage);
	//cvImage *= 1.5;
	

	for (size_t i = 0; i < cvImage.rows; i++)
		for (size_t j = 0; j < cvImage.cols; j++) {
			cv::Vec3b& color = cvImage.at<cv::Vec3b>(j, i);
			float contrast = 1.5;
			color.val[0] = min((float)contrast * color.val[0], 255.f);
			color.val[1] = min((float)contrast * color.val[1], 255.f);
			color.val[2] = min((float)contrast * color.val[2], 255.f);
		}
	
	
	img = getOfImageFromCvImage(cvImage);
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofDrawBitmapString("Test text", 10, 10);

	refImage.draw(15,10);
	img.draw(img.getWidth()+20,10);

	//Draw circle on black font
	cv::Mat blackFont(300, 300, CV_8UC3, cv::Scalar(0, 0, 0));
	//cv::circle(blackFont, cv::Point(blackFont.cols / 2 - blackFont.cols / 4, blackFont.rows / 2), 0.0, cv::Scalar(255, 255, 255), 5, cv::LINE_8);
	//cv::circle(blackFont, cv::Point(blackFont.cols / 2 + blackFont.cols / 4, blackFont.rows / 2), 0.0, cv::Scalar(0, 0, 255), 5, cv::LINE_4);
	
	DrawCircle(blackFont, 50.0, 100.0, 100.0, 1.f, cv::Scalar(0, 255, 255));

	
	getOfImageFromCvImage(blackFont).draw(15, img.getHeight() + 15);
}

void ofApp::DrawCircle(cv::Mat& picture, float radius, float x, float y, float thick, cv::Scalar circleColor)
{
	for (size_t i = 0; i < picture.rows; i++)
		for (size_t j = 0; j < picture.cols; j++) {
			cv::Vec3b& color = picture.at<cv::Vec3b>(j, i);
			if (sqrt((i - x) * (i - x) + (j - y) * (j - y)) - radius <= 0.f &&
				sqrt((i - x) * (i - x) + (j - y) * (j - y)) - radius + thick >= 0.f)
			{
				color.val[0] = circleColor.val[0];
				color.val[1] = circleColor.val[1];
				color.val[2] = circleColor.val[2];
			}
		}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
cv::Mat ofApp::getCvMatFromOfImage(ofImage& img) {
	auto cvImg = cv::Mat(img.getHeight(), img.getWidth(), CV_MAKETYPE(CV_8U, img.getPixels().getNumChannels()), img.getPixels().getData());
	return cvImg.clone();
}
ofImage ofApp::getOfImageFromCvImage(cv::Mat& mat) {
	ofImage img;
	if (mat.channels() == 3) img.setFromPixels(mat.data, mat.cols, mat.rows, OF_IMAGE_COLOR);
	if (mat.channels() == 4) img.setFromPixels(mat.data, mat.cols, mat.rows, OF_IMAGE_COLOR_ALPHA);
	if (mat.channels() == 1) img.setFromPixels(mat.data, mat.cols, mat.rows, OF_IMAGE_GRAYSCALE);

	return img;
}